v Hij maakt niet zelf een /tmp folder
v Solenoid maken
v Illegale tekens in functie block naam affilteren (-)
v Klep ID (A, B, C) in beschrijving zetten
v Analog outputs optioneel maken

v Er wordt geen main program meer aangemaakt met de standaard programma's
v In programma Piezo's staat niks
v In programma temperature staat niks
v X24 piezo knop was leeg in comments
v In kleppen programma zijn de klep functieblokken niet goed uitgevoerd. Je krijgt een melding dat het niet het juiste aantal in/uit gangen zijn.
v Function block maximal_thermostat niet in file
v Function block PI_control niet in file
v Heaters in eigen functieblok, niet in standaard device. Moet ten alle tijden een flow en circ pump beveiliging in.

v In klep modussen een mogelijkheid maken om kleppen open te draaien maar niet mee te nemen met de requirements
v Solenoid terugmelding Y voor gebruiken in modussen
v Common alarm
v Start of functionblock name is not a letter or underline
- Devices naam uniek maken
v Change name order of valve feedback bits (M820 etc) to increase readability
v Error handling per POU
v Function block identifier name filter for reserved symbols (M, D, etc)
v Frequency controlled pumps
