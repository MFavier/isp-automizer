When you export something which contains a function block you need to import the function blocks separately
[LD_NODE]
types:
1:LD
2:LDI
5:
6:Parallel block
7:End of output block
9:Block
11:Input
12:Output
13:OUT
20:Empty space filler for parallel output blocks
29:Branch block. LNK_C is block height, LNK_L is block depth

Activate multiple POUs via Programs->Right click->Task Property->Cyclic Task->Assigned POUs

E900 and similar are system reserved identifiers
