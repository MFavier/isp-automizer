# ISP Automizer

This project has a purpose of automating the creation of PLC software for Delta ISPSoft. It does this by generating files which can be imported into an existing ISP project.
Using an excel file as reference, you can import one file for any number of POUs generated, and one file for importing device comments.

Workflow:
1. Make a Devices.xlsx for your specific project. Put this in the main folder of isp-Automizer. Please ensure all valves, pumps and other devices have unique IDs
2. Change Config.txt for your specific needs
3. Run main.py, this creates an .MPU file and a .rcm file in the tmp/ folder
4. Open your template n ISPSoft and import these files via File -> Import. 'Import programs' imports the .MPU file and 'Import device comments' imports the .rcm file. The password is always 1234
5. Import Function blocks from examples/Automizer.FBU
6. Activate programs via Programs/Right click/Task property. Move Unassigned POUs to Assigned POUs under Cyclic Task


Python libraries required:
pandas
openpyxl
configparser
