from src.MPU import makeMPU
from src.writeISP import makeZip
from src.Valves import makeValves
from src.Ports import readPorts, writePorts, updatePD
from src.Devices import makeDevices
from src.Levels import makeLevels
from src.Analog import makeAnalogInputs, makeAnalogOutputs
from src.Modes import makeChecks, makeValvePositions
from src.Digital import makeDigitalInputs, makeDigitalOutputs
from src.Freq import makeFreqPumps
import configparser
import datetime

today = (str(datetime.date.today().year) + f"{(datetime.date.today().month):02d}" + f"{(datetime.date.today().day):02d}")
todayNice = (f"{(datetime.date.today().day):02d}" + "-" + f"{(datetime.date.today().month):02d}" + "-" + str(datetime.date.today().year))
print(today)
print(todayNice)

##Read config from config file
configParser = configparser.RawConfigParser()
configParser.read('Config.txt')
#fileName = str(os.path.dirname(os.path.abspath(__file__))) + '\\Templates\\' + configParser.get('config', 'libraryFile')
#plc = fileName[-3:]
#print("Using " + fileName + " as template")
excelFile = configParser.get('config', 'excelFile')
#title = configParser.get('config', 'projectName') + "\r\nStarted " + todayNice
#doTitle = configParser.get('config', 'doTitle')
#doTitle = True if doTitle == 'True' else False
doPrograms = configParser.get('config', 'doPrograms')
doPrograms = True if doPrograms == 'True' else False
doValves = configParser.get('config', 'doValves')
doValves = True if doValves == 'True' else False
doDevices = configParser.get('config', 'doDevices')
doDevices = True if doDevices == 'True' else False
doErrorManStart = configParser.get('config', 'doErrorManStart')
doErrorManStart = True if doErrorManStart == 'True' else False
doAnalog = configParser.get('config', 'doAnalog')
doAnalog = True if doAnalog == 'True' else False
doDigital = configParser.get('config', 'doDigital')
doDigital = True if doDigital == 'True' else False
doLevels = configParser.get('config', 'doLevels')
doLevels = True if doLevels == 'True' else False
doFreqs = configParser.get('config', 'doFreqs')
doFreqs = True if doFreqs == 'True' else False
#outputName = str(today) + ' - ' + configParser.get('config', 'projectName') + '.' + plc



head, body = readPorts()

valves = []
try:
    if doValves:
        valves = makeValves(excelFile)
        for valve in valves:
            for port in valve.ports:
                updatePD(body, port)
except Exception as e:
    print("Error in valves")
    print(e)
    quit()


devices = []
devicesTemp = []
heaters = []
try:
    if doDevices:
        devicesTemp = makeDevices(excelFile)
        for device in devicesTemp:
            for port in device.ports:
                updatePD(body, port)
        for device in devicesTemp:
            if device.type == 'Heater':
                heaters.append(device)
            else:
                devices.append(device)
except:
    print("Error in devices")
    quit()

levels = []
try:
    if doLevels:
        levels = makeLevels(excelFile)
        for level in levels:
            for port in level.ports:
                updatePD(body, port)
except:
    print("Error in levels")
    quit()

analogInputs = []
analogOutputs = []
if doAnalog:
    try:
        analogInputs = makeAnalogInputs(excelFile)
        for port in analogInputs.ports:
            updatePD(body, port)
    except:
        print("Error in analog inputs")
        quit()
    try:
        analogOutputs = makeAnalogOutputs(excelFile)
        if analogOutputs:
            for port in analogOutputs.ports:
                    updatePD(body, port)
    except:
        print("Error in analog outputs")
        quit()


modesIn = []
modesOut = []
try:
    if doPrograms:
        modesIn = makeChecks(excelFile)
        modesOut = makeValvePositions(excelFile)
        for mode in modesIn:
            for port in mode.ports:
                updatePD(body, port)
except Exception as e:
    print("Error in modes")
    print(e)
    quit()

commonAlarm = ''
if doDigital:
    digitalInputs = makeDigitalInputs(excelFile)
    digitalOutputs = makeDigitalOutputs(excelFile)
    try:
        for input in digitalInputs:
            for port in input.ports:
                updatePD(body, port)
    except:
        print("Error in digital inputs")
        quit()
    try:
        for output in digitalOutputs:
            if 'common alarm' in output.description.lower():
                commonAlarm = output.ports[0]
            for port in output.ports:
                updatePD(body, port)
    except:
        print("Error in digital outputs")
        quit()

freqPumps = []
if doFreqs:
    try:
        freqPumps = makeFreqPumps(excelFile)
        for pump in freqPumps:
            for port in pump.ports:
                updatePD(body, port)
    except Exception as e:
        print("Error in frequency controlled pumps")
        print(e)
        quit()

makeMPU(valves=valves, devices=devices, heaters=heaters, levels=levels,  analogInputs=analogInputs, analogOutputs=analogOutputs, modesIn=modesIn, modesOut=modesOut, commonAlarm=commonAlarm, freqPumps=freqPumps)
makeZip()
writePorts(head, body)

#from src.readISP import readMPU
#
#readMPU('ErrorsManuals')
