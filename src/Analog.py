from src.rwExcel import readAnalogExcel, readAnalogOutputsExcel
from src.readISP import PortDescription, isConsecutive

def PLCAnalogSettings(fileName):
    excelInput = readAnalogExcel(fileName)
    excelOutput = readAnalogOutputsExcel(fileName)
    setting = 0
    for index, row in excelInput.iterrows():
        if index < 4:
            try:
                int(row["Extension"])
            except ValueError:
                if(row["V/I"]) == 'I':
                    setting += 2**int(row["Channel"])
    for index, row in excelOutput.iterrows():
        if index < 2:
            try:
                int(row["Extension"])
            except ValueError:
                if(row["V/I"]) == 'I':
                    setting += 2**(int(row["Channel"]) + 4)
    return setting

def inputExtensionSettings(fileName,  extension):
    excelInput = readAnalogExcel(fileName)    
    setting = 0
    for index, row in excelInput.iterrows():
        try:
            if(row["V/I"]) == 'I' and int(row["Extension"]) == extension:
                setting += 3*8**(int(row["Channel"])-1)
        except ValueError:
            pass
    return(setting)

def outputExtensionSettings(fileName,  extension):
    excelOutput = readAnalogOutputsExcel(fileName)    
    setting = 0
    for index, row in excelOutput.iterrows():
        try:
            if(row["V/I"]) == 'I' and int(row["Extension"]) == extension:
                setting += 3*8**(int(row["Channel"])-1)
        except ValueError:
            pass
    return(setting)

class AnalogInput():
    def __init__(self,  row):
        self.ports = []
        if type(row["Location"]) is str:
            self.location = row["Location"]
        self.function = row["Function"]
        if type(row["Subject"]) is str:
            self.subject = row["Subject"]
        if type(row["ID"]) is str:
            self.ID = row["ID"]
        self.description = (str(self.ID) + ", " if hasattr(self,  'ID') else "") + self.function + (" " + self.location.lower() if hasattr(self,  'location') else " ") + (("of " + self.subject.lower()) if hasattr(self,  'subject') else "")
        self.VI = row["V/I"]
        try:
            self.extension = int(row["Extension"])
            if self.VI == 'I':
                self.resolutionMin = 800
                self.resolutionMax = 4000
            else:
                self.resolutionMin = 0
                self.resolutionMax = 8000
            self.onPLC = False
        except ValueError:
            self.resolutionMin = 400
            self.resolutionMax = 2000
            self.onPLC = True
        self.channel = int(row["Channel"])
        self.Doutput = int(row["D output"])
        self.ports.append(PortDescription('D', self.Doutput, self.description))
        self.Dinput = int(row["D input"])
        if not self.onPLC:
            self.ports.append(PortDescription('D', self.Dinput, "Input card " + str(self.extension) + "-" + str(self.channel) + " - " + self.description))
        self.Ttimeout = int(row["T timeout"])
        self.ports.append(PortDescription('T',  self.Ttimeout, "Timeout " + self.description))
        self.Merror = int(row["M error"])
        self.ports.append(PortDescription('M',  self.Merror, "Analog error " +  self.description))
        try:
            self.lowest = int(row["Lowest value"])
            self.highest = int(row["Maximum value"])
        except ValueError:
            raise ValueError("Error boundary values analog inputs")
        self.slope = (self.highest - self.lowest)/(self.resolutionMax - self.resolutionMin)
        self.slopeValue = int(1000*self.slope)
        self.offset = int(self.lowest - (self.resolutionMin * self.slope))
        self.timerName = f"Timer_{'PLC' if self.onPLC else self.extension}_{self.channel}"
        self.varString = f"""{{
{self.timerName} : AnalogTimer :=  [@@] {{VAR}}
(*

*)
}}
"""


class AnalogInputsBlock():
        def __init__(self,  analogInputs, fileName):
            self.ports = []
            self.varString = "<VAR>\n"
            for input in analogInputs:
                self.ports += input.ports
                self.varString += input.varString
            self.varString += "</VAR>\n"
            self.extensionNos = []
            for input in analogInputs:
                try:
                    if input.extension not in self.extensionNos:
                        self.extensionNos.append(input.extension)
                except AttributeError:
                    pass
            self.extensionNos.sort()
            self.extensionList = []
            for extension in self.extensionNos:
                self.thisExtension = []
                for input in analogInputs:
                    try:
                        if input.extension == extension:
                            self.thisExtension.append(input)
                    except AttributeError:
                        pass
                self.extensionList.append(self.thisExtension)
            plcInputs = [x for x in analogInputs if x.onPLC]
            if len(plcInputs) > 0:
                self.dataString = f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID=1
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
Sensors
Analog inputs on PLC
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1002
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=3000
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=MOV
DEV_NAME=
API_NUM=28
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D1118
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={PLCAnalogSettings(fileName)}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=MOV
DEV_NAME=
API_NUM=28
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D1115
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=29
LNK_C=2
LNK_L=3
[END_LD_NODE]
[LD_NODE]
TYPE=3
DEV_NAME=M1013
[END_LD_NODE]
"""                
                for i,  input in enumerate(plcInputs):
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{input.Dinput}
VAR_NAME={input.slopeValue}
VAR_NAME={input.offset}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=SCAL
DEV_NAME=
API_NUM=394
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{input.Doutput}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={str(len(plcInputs))}
LNK_L=3
[END_LD_NODE]
"""
                for i,  input in enumerate(plcInputs):
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{input.Dinput}
VAR_NAME=300
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=8
SYMB=AnalogTimer
DEV_NAME={input.timerName}
API_NUM=-1
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=M{input.Merror}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
            self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={len(plcInputs)+2}
LNK_L=4
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
""" #LNK_C seems to be the number of end nodes 
            for extension in self.extensionList:
                self.dataString += f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID={extension[0].extension + 2}
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
Analog input card
Extension {extension[0].extension}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1002
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={extension[0].extension}
VAR_NAME=1
VAR_NAME={inputExtensionSettings(fileName, extension[0].extension)}
VAR_NAME=1
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=TO
DEV_NAME=
API_NUM=224
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=0
VAR_NAME=4
VAR_NAME=50
VAR_NAME=4
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=TO
DEV_NAME=
API_NUM=224
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=29
LNK_C=2
LNK_L=3
[END_LD_NODE]
[LD_NODE]
TYPE=3
DEV_NAME=M1013
[END_LD_NODE]
"""
                channels = [channel.channel for channel in extension]
                inputs = [channel.Dinput for channel in extension]
                fromLen = 0
                if isConsecutive(channels) and isConsecutive(inputs):
                    fromLen = 1
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={extension[0].extension}
VAR_NAME={extension[0].channel + 5}
VAR_NAME={len(inputs)}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=FROM
DEV_NAME=
API_NUM=220
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{inputs[0]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                else:
                    for channel in extension:
                        fromLen = len(extension)
                        self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={channel.extension}
VAR_NAME={channel.channel+5}
VAR_NAME=1
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=FROM
DEV_NAME=
API_NUM=220
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{channel.Doutput}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""

                for channel in extension:
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{channel.Dinput}
VAR_NAME={channel.slopeValue}
VAR_NAME={channel.offset}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=SCAL
DEV_NAME=
API_NUM=394
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{channel.Doutput}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={len(extension) + fromLen}
LNK_L=3
[END_LD_NODE]
"""
                for i, channel in enumerate(extension):
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{channel.Dinput}
VAR_NAME=700
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=8
SYMB=AnalogTimer
DEV_NAME={channel.timerName}
API_NUM=-1
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=M{channel.Merror}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
                self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={len(extension)+2}
LNK_L=4
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""


def makeAnalogInputs(fileName):
    df = readAnalogExcel(fileName)
    inputs = []
    for index,  row in df.iterrows():
        if type(row["Function"]) is str:
            inputs.append(AnalogInput(row))
    return AnalogInputsBlock(inputs, fileName)

class AnalogOutput():
    def __init__(self,  row):
        self.ports = []
        self.ID = row["ID"]
        if type(row["Location"]) is str:
            self.location = row["Location"]        
        self.function = row["Function"]
        self.VI = row["V/I"]
        self.description = ((str(self.ID) + ", ") if hasattr(self, 'id') else "") + str(self.function) + (" at " + str(self.location) if hasattr(self, 'location') else "")
        try:
            self.extension = int(row["Extension"])
            self.onPLC = False
        except ValueError:
            self.onPLC = True
        self.channel = int(row["Channel"])
        self.Dinput = int(row["D input"])
        self.ports.append(PortDescription("D", self.Dinput, "Setpoint analog output " + self.description))
        self.Doutput = int(row["D output"])
        self.ports.append(PortDescription("D",  self.Doutput, "Analog output " + self.description))
        self.inputMin = int(row["Input min"])
        self.inputMax = int(row["Input max"])
        if self.VI == 'V' and self.onPLC:
            self.outputHigh = 2000
            self.outputLow = 400
        else:
            self.outputHigh = 4000
            self.outputLow = 800
        self.slope = (self.outputHigh - self.outputLow) / (self.inputMax - self.inputMin)
        self.slopeValue = int(self.slope*1000)
        self.offset = int(self.outputLow - (self.inputMin*self.slope))

class AnalogOutputsBlock():
    def __init__(self,  outputs, fileName):
        self.ports = []
        if len(outputs) > 0:
            self.extensionNos = []
            for output in outputs:
                self.ports += output.ports
                try:
                    if output.extension not in self.extensionNos:
                        self.extensionNos.append(output.extension)
                except AttributeError:
                    pass
            self.extensionNos.sort()
            self.extensionList = []
            for extension in self.extensionNos:
                self.thisExtension = []
                for output in outputs:
                    try:
                        if output.extension == extension:
                            self.thisExtension.append(output)
                    except AttributeError:
                        pass
                self.extensionList.append(self.thisExtension)
            plcOutputs = [x for x in outputs if x.onPLC]
            if len(plcOutputs) > 0:
                self.dataString = f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID={4}
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
Analog outputs
Analog outputs PLC
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
""" 
                for output in plcOutputs:
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{output.Dinput}
VAR_NAME={output.slopeValue}
VAR_NAME={output.offset}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=SCAL
DEV_NAME=
API_NUM=394
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{output.Doutput}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                if len(plcOutputs) > 1:
                    self.dataString += """[LD_NODE]
TYPE=29
LNK_C=2
LNK_L=3
[END_LD_NODE]
"""
                self.dataString += """<OUTLINK_END>
<NETWORK_END>
"""
            for extension in self.extensionList:
                self.dataString += f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID={5}
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
Analog output card
Extension {extension[0].extension}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1002
[END_LD_NODE]
<OUTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1002
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={extension[0].extension}
VAR_NAME=1
VAR_NAME={outputExtensionSettings(fileName, extension[0].extension)}
VAR_NAME=1
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=TO
DEV_NAME=
API_NUM=224
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                inputs = [x.Dinput for x in extension]
                outputs = [x.Doutput for x in extension]
                blockLen = 0
                if isConsecutive(inputs) and isConsecutive(outputs):
                    blockLen = len(outputs) + 2
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={extension[0].extension}
VAR_NAME={extension[0].channel + 5}
VAR_NAME=D{extension[0].Dinput}
VAR_NAME={len(inputs)}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=TO
DEV_NAME=
API_NUM=224
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
                else:
                    blockLen = 2*len(outputs) + 1
                    for channel in extension:
                        self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME={channel.extension}
VAR_NAME={channel.channel + 5}
VAR_NAME=D{channel.Dinput}
VAR_NAME=1
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=TO
DEV_NAME=
API_NUM=224
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
                for channel in extension:
                    self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{channel.Dinput}
VAR_NAME={channel.slopeValue}
VAR_NAME={channel.offset}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=SCAL
DEV_NAME=
API_NUM=394
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{channel.Doutput}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
                self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={blockLen}
LNK_L=4
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""


def makeAnalogOutputs(fileName):
    df = readAnalogOutputsExcel(fileName)
    outputs = []
    for index,  row in df.iterrows():
        if type(row["Function"]) is str:
            outputs.append(AnalogOutput(row))
    return AnalogOutputsBlock(outputs, fileName)
    
