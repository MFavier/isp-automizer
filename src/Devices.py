from src.readISP import PortDescription
from src.rwExcel import readDevicesExcel


reserved = ['X', 'Y', 'M', 'S', 'T', 'C', 'D', 'P', 'I', 'E']

# inputs & outputs are a list of PortDescriptions
class FunctionBlock():
    def __init__(self):
        self.name = ''
        self.description = ''
        self.inputs = []
        self.outputs = []
        self.index = 0
        self.type = ''
    def makeBlock(self):
        self.name = self.name.replace('-', '_').replace('.', '_')
        if (not self.name[0].isalpha()) or self.name[0] in reserved:
            self.name = '_' + self.name
        self.dataString = f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID={self.index}
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
{self.description}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
"""
        for input in self.inputs:
            self.dataString += f'VAR_NAME={input.letter}{str(input.id)}\n'
        self.dataString += f"""<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=8
SYMB={self.type}
DEV_NAME={str(self.name)}
API_NUM=-1
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
"""
        for output in self.outputs:
            self.dataString += f'VAR_NAME={output.letter}{str(output.id)}\n'
        self.dataString += """<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
<OUTLINK_END>
<NETWORK_END>
"""
        self.varString = f"""{{
{self.name} : {self.type} :=  [@@] {{VAR}}
(*

*)
}}
"""

def deviceString(devices):
    data = ""
    for device in devices:
        data = data + device.dataString
    return data

def deviceVarString(devices):
    data = "<VAR>\n"
    for device in devices:
        data = data + device.varString
    data += "</VAR>\n"
    return data



class Device(FunctionBlock):
    def __init__(self, df, rowId, deviceID):
        FunctionBlock.__init__(self)
        self.index = deviceID
        self.location = df.at[rowId, 'Location']
        self.function = df.at[rowId, 'Function']
        self.id = int(df.at[rowId, 'ID'])
        self.deviceid = str(df.at[rowId, 'Device ID'])
        self.name = self.deviceid
        self.description = f'{self.deviceid}\n{self.function} {self.id}'
        self.outputs.append(PortDescription('Y', int(df.at[rowId, 'Y']), self.function + " " + str(self.id) + " - " + str(self.deviceid)))
        self.outputs.append(PortDescription('D', int(df.at[rowId, 'D status']), self.function + " " + str(self.id) + " state - " + str(self.deviceid)))
        self.outputs.append(PortDescription('M', int(df.at[rowId, 'M manual']), self.function + " " + str(self.id) + " MANUAL - " + str(self.deviceid)))
        self.inputs.append(PortDescription('M', int(df.at[rowId, 'M OFF']), self.function + " " + str(self.id) + " OFF - " + str(self.deviceid)))
        self.inputs.append(PortDescription('M', int(df.at[rowId, 'M ON']), self.function + " " + str(self.id) + " ON - " + str(self.deviceid)))
        self.inputs.append(PortDescription('M', int(df.at[rowId, 'M AUTO']), self.function + " " + str(self.id) + " AUTO - " + str(self.deviceid)))
        self.inputs.append(PortDescription('M', int(df.at[rowId, 'M Prog']), self.function + " " + str(self.id) + " Prog - " + str(self.deviceid)))
        #self.Mflow = df.at[rowId, 'M flow error']
        try:
            if str(df.at[rowId, 'X error'])[0] == "!":
                self.inputs.append(PortDescription('X', int(df.at[rowId, 'X error'][1:]), "Error N.C. - " + self.function + " " + str(self.id) + " - " + str(self.deviceid)))
                self.type = 'Pump'
            else:
                self.inputs.append(PortDescription('X', int(df.at[rowId, 'X error']), "Error N.O. - " + self.function + " " + str(self.id) + " - " + str(self.deviceid)))
                self.type = 'Pump'
        except (KeyError, ValueError):
            self.type = 'Device'
        if self.function == 'Heater':
            self.inputs.append(PortDescription('M', 0, ''))
            self.type = 'Heater'
        try:
            if self.type == 'Pump':
                self.outputs.append(PortDescription('M', int(df.at[rowId, 'M failure']), "Failure " + self.function + " " + str(self.id) + " - " + str(self.deviceid)))
        except ValueError:
            pass
        self.ports = self.inputs + self.outputs
        self.makeBlock()




def makeDevices(fileName):
    df = readDevicesExcel(fileName)
    devices = []
    deviceID = 0
    for index,  row in df.iterrows():
        try:
            int(row.Y)
            deviceID += 1
            devices.append(Device(df, index, deviceID))
        except ValueError:
            continue
    return devices


