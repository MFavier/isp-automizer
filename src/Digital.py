from src.readISP import PortDescription
from src.rwExcel import readDigitalInputsExcel, readDigitalOutputsExcel
import math

class DigitalInput():
    def __init__(self,  row):
        self.ports = []
        if type(row["ID"]) is str:
            self.ID = row["ID"] 
        if type(row["Description"]) is str:
            self.description = row["Description"]
        if type(row["Location"]) is str:
            self.location = row["Location"]
        if type(row["Type"]) is str:
            self.type = row["Type"]
        self.X = int(row["X"])
        self.text = (self.ID + ", " if hasattr(self, "ID") else "") + (self.description + " " if hasattr(self, "description") else "") + ((self.type.lower() if hasattr(self, "description") else self.type) if hasattr(self, "type") else "") + (" at " + self.location.lower() if hasattr(self, "location") else "")
        self.ports.append(PortDescription("X", self.X, self.text))

def makeDigitalInputs(fileName):
    df = readDigitalInputsExcel(fileName)
    inputs = []
    for index,  row in df.iterrows():
        if not math.isnan(row["X"]):
            inputs.append(DigitalInput(row))
    return inputs

class DigitalOutput():
    def __init__(self,  row):
        self.ports = []
        if type(row["ID"]) is str:
            self.ID = row["ID"] 
        if type(row["Description"]) is str:
            self.description = row["Description"]
        if type(row["Location"]) is str:
            self.location = row["Location"]
        if type(row["Type"]) is str:
            self.type = row["Type"]
        self.Y = int(row["Y"])
        self.text = (self.ID + ", " if hasattr(self, "ID") else "") + (self.description + " " if hasattr(self, "description") else "") + ((self.type.lower() if hasattr(self, "description") else self.type) if hasattr(self, "type") else "") + (" at " + self.location.lower() if hasattr(self, "location") else "")
        self.ports.append(PortDescription("Y", self.Y, self.text))

def makeDigitalOutputs(fileName):
    df = readDigitalOutputsExcel(fileName)
    outputs = []
    for index,  row in df.iterrows():
        if not math.isnan(row["Y"]):
            outputs.append(DigitalOutput(row))
    return outputs
