from src.rwExcel import readFreqExcel
from src.readISP import PortDescription

def makeFreqPumps(fileName):
    df = readFreqExcel(fileName)
    freqs = {}
    pumps = []
    for index,  row in df.iterrows():
        if type(row["Device ID"]) is str:
            if row["Device ID"] != '-':
                if row["Device ID"] in freqs:
                    freqs[row["Device ID"]].append(row)
                else:
                    freqs[row["Device ID"]] = [row]
    for key, value in freqs.items():
        pump = FreqPump(value)
        pumps.append(pump)
    return pumps


class FreqPump():
    def __init__(self, device):
        self.id = device[0]['Device ID']
        self.function = device[0]['Function']
        self.description = self.function + " " + self.id
        self.registers = []
        self.modes = []
        self.ports = []
        for key in device[0].keys():
            if key[0:5] == "M req":
                self.registers.append(key)
        for mode in device:
            if type(mode["Function"]) is str and type(mode["Mode"]) is str:
                for key in self.registers:
                    if mode[key] != "!Error!" and mode[key] != "-":
                        self.modes.append(mode)
                        self.ports.append(PortDescription("D",  mode["D setpoint"], "Setpoint " + mode["Mode"] + " " + mode["Function"] + " " + mode["Device ID"]))
                        break
        self.depth = 0
        for register in self.registers:
            for mode in device:
                try:
                    int(mode[register])
                    self.depth += 1
                    break
                except:
                    pass
        self.dataString = f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID=1
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=TRUE
NET_FOLD=TRUE
(*
Speed {self.description}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=2
DEV_NAME=Y{device[0]["Y"]}
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=0
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=MOV
DEV_NAME=
API_NUM=28
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{device[0]["D output"]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
        i = 1
        while i < self.depth:
            i += 1
            self.dataString += """[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
        for mode in self.modes:
            if mode["Mode"] == "Manual":
                self.dataString += f"""[LD_NODE]
TYPE=1
DEV_NAME=M{mode["M req 1"]}
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{mode["D setpoint"]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=MOV
DEV_NAME=
API_NUM=28
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{mode["D output"]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                i = 1
                while i < self.depth:
                    i += 1
                    self.dataString += """[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
            else:
                j = 0
                parallel  = 0
                for register in self.registers:
                    output = 0
                    try:
                        if str(mode[register])[0] == "|":
                            output = int(mode[register][1:])
                            parallel += 1
                        else:
                            output = int(mode[register])
                        if mode[register] != "-":
                            if parallel < 2:
                                j += 1
                            self.dataString += f"""[LD_NODE]
TYPE=1
DEV_NAME=M{output}
[END_LD_NODE]
"""
                    except ValueError:
                        pass
                if parallel > 1:
                    self.dataString += f"""[LD_NODE]
TYPE=6
LNK_C={parallel}
LNK_L=1
[END_LD_NODE]
"""
                self.dataString += f"""[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{mode["D setpoint"]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=9
SYMB=MOV
DEV_NAME=
API_NUM=28
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=D{mode["D output"]}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
"""
                while j < self.depth:
                    j += 1
                    self.dataString += """[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
"""
        self.dataString += f"""[LD_NODE]
TYPE=29
LNK_C={len(self.modes) + 1}
LNK_L={self.depth + 3}
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""
