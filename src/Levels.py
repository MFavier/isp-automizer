from src.rwExcel import readLevelsExcel
from src.readISP import PortDescription


class Level():
    def __init__(self, id, df):
        self.ports = []
        self.id = id
        self.System = df.at[self.id,  "System"]
        self.name = df.at[self.id,  "Name"]
        self.Dsensor = df.at[self.id,  "D sensor"]
        self.greaterSmaller = df.at[self.id,  "<>="]
        self.Dsetpoint = df.at[self.id,  "D setpoint"]
        self.Kset = df.at[self.id,  "K set"]
        self.Kreset = df.at[self.id,  "K reset"]
        self.Msetpoint = df.at[self.id,  "M setpoint"]
        self.Mthreshold = df.at[self.id, "M threshold"]
        self.type = 'Level'
        self.description = str(self.System) + " - " + str(self.name)
        self.ports.append(PortDescription('D', self.Dsetpoint, "lvl " + self.description))
        self.ports.append(PortDescription('M', self.Msetpoint, self.description))
        self.ports.append(PortDescription('M', self.Mthreshold, self.description))
        self.dataString = ""
        self.varString = ""
    def makeString(self):
        self.identity = self.name.replace(' ',  '_')
        self.dataString=f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID=1
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=TRUE
NET_FOLD=TRUE
(*
Level {self.System} - {self.name}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M1000
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=D{self.Dsensor}
VAR_NAME=D{self.Dsetpoint}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=10
SYMB={self.greaterSmaller}
DEV_NAME=
API_NUM=-1
[END_LD_NODE]
[LD_NODE]
TYPE=12
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=13
DEV_NAME=M{self.Mthreshold}
[END_LD_NODE]
[LD_NODE]
TYPE=11
<VAR_NODE_S>
VAR_NAME=M{self.Mthreshold}
VAR_NAME={self.Kset}
VAR_NAME={self.Kreset}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=8
SYMB=Level
DEV_NAME={self.identity}
API_NUM=-1
[END_LD_NODE]
[LD_NODE]
TYPE=12
<VAR_NODE_S>
VAR_NAME=M{self.Msetpoint}
<VAR_NODE_E>
[END_LD_NODE]
[LD_NODE]
TYPE=7
[END_LD_NODE]
[LD_NODE]
TYPE=20
DEV_NAME=
[END_LD_NODE]
[LD_NODE]
TYPE=29
LNK_C=2
LNK_L=4
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""
        self.varString = f"""{{
{self.identity} : {self.type} :=  [@@] {{VAR}}
(*

*)
}}
"""


def makeLevels(fileName):
    df = readLevelsExcel(fileName)
    levels = []
    for index,  row in df.iterrows():
        if(row.Include == 'v'):
            levels.append(Level(index, df))
    nameList = []
    for i, level in enumerate(levels):
            if not level.name in nameList:
                nameList.append(level.name)
            else:
                appendix = 2
                while(True):
                    newName = level.name + str(appendix)
                    if not newName in nameList:
                        levels[i].name = newName
                        nameList.append(newName)
                        break
                    else:
                        appendix += 1
    for i, level in enumerate(levels):
        levels[i].makeString()
    return levels
