from src.Devices import deviceString, deviceVarString
from src.POU import backwash, comm, errorsManuals
from src.readISP import PortDescription

#TODO: Common alarm

def makeMPU(commonAlarm, valves=[], devices=[], heaters=[], levels=[], analogInputs=[], analogOutputs=[], modesIn=[], modesOut=[], freqPumps=[]):
    f =  open('.\\tmp\\Unzipped.src', 'w')
    f.write('<GroupPOUFolder>\n')
    f.write('<ProgramFolder>\n')
    POUs = []
    if valves:
        POUs.append('Valves')
    if devices:
        POUs.append('Devices')
    if heaters:
        POUs.append('Heaters')
    if levels:
        POUs.append('Levels')
    if analogInputs or analogOutputs:
        POUs.append('Analog')
    POUs.append('IntCommunication')
    if modesIn: 
        POUs.append('ModesInput')
    if modesOut:
        POUs.append('ModesOutput')
    if freqPumps:
        POUs.append('Freqpumps')
    POUs.append('ErrorsManuals')
    POUs.append('Backwash')
    #POUs.append('Temperature')
    #POUs.append('Piezos')
    for pou in POUs:
        f.write(f"""<FolderContent>
ContentType=0
ContentName={pou} [PRG,LD]
ContentPath=Program/{pou} [PRG,LD]
</FolderContent>
""")
    f.write('<\\ProgramFolder>\n')
    f.write('<\\GroupPOUFolder>\n')
    for pou in POUs:
        f.write(f"""<POU>
P_Name={pou}
P_En_Eno=TRUE
P_Last_Chg=
P_type=0
P_Rtn_Type=
P_Lang=1
P_Step=0
P_Version=1.00
P_DeltaFB=FALSE
P_Security=
P_Active=TRUE
P_Priority=9999
P_DFBName=
(*
Automatically generated
*)
<LOCAL_VAR>
""")
        match pou:
            case 'Valves':
                f.write(deviceVarString(valves))
            case 'Devices':
                f.write(deviceVarString(devices))
            case 'Heaters':
                f.write(deviceVarString(heaters))
            case 'Levels':
                f.write(deviceVarString(levels))
            case 'Analog':
                f.write(analogInputs.varString)
        f.write("""</LOCAL_VAR>
<VAR_EXTERN>
</VAR_EXTERN>
<VAR_EXTERN_C>
</VAR_EXTERN_C>
""")
        match pou:
            case 'Valves':
                f.write(deviceString(valves))
            case 'Devices':
                f.write(deviceString(devices))
            case 'Heaters':
                f.write(deviceString(heaters))
            case 'Levels':
                f.write(deviceString(levels))
            case 'Backwash':
                f.write(backwash())
            case 'IntCommunication':
                f.write(comm())
            case 'ErrorsManuals':
                if commonAlarm:
                    f.write(errorsManuals(commonAlarm))
                else:
                    f.write(errorsManuals(PortDescription('Y', 0, 'Common alarm')))
            case 'Analog':
                f.write(analogInputs.dataString)
                if hasattr(analogOutputs, 'dataString'):
                    f.write(analogOutputs.dataString)
            case 'ModesInput':
                f.write(deviceString(modesIn))
            case 'ModesOutput':
                f.write(deviceString(modesOut))
            case 'Freqpumps':
                f.write(deviceString(freqPumps))
        f.write('</POU>\n')
    f.close()

