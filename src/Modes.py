from src.rwExcel import readModesExcel, readValvesExcel
from src.readISP import PortDescription
#import pandas as pd

##Keep track of bridges
bridges = []
for i in range(41):
    bridges.append(3400+i)


class PositionCheck():
    def __init__(self,  valves,  mode):
        self.ports = []
        self.ports.append(PortDescription('M', int(mode["M MODE"]),  "MODE: " + (mode["System"] + " - " if type(mode["System"]) is str else "") + mode["Mode name"]))
        self.ports.append(PortDescription('M', int(mode["M PP"]),  "PP: " + (mode["System"] + " - " if type(mode["System"]) is str else "") + mode["Mode name"]))
        self.ports.append(PortDescription('M', int(mode["M PROG"]),  "PROG: " + (mode["System"] + " - " if type(mode["System"]) is str else "") + mode["Program"]))
        self.ports.append(PortDescription('M', int(mode["M STATE"]),  "STATE: " + (mode["System"] + " - " if type(mode["System"]) is str else "") + mode["Mode name"]))
        openValves = str(mode["Valves open"]).split()
        closedValves = str(mode["Valves closed"]).split()
        registers = []
        self.dataString = ""
        for valve in closedValves:
            for reference in valves:
                if reference[1] == valve:
                    if reference[5] == "Solenoid":
                        registers.append("2\nDEV_NAME=Y" + str(reference[6]))
                    else:
                        registers.append("1\nDEV_NAME=M" + str(reference[2]))
        for valve in openValves:
            if valve[0] != '!':
                for reference in valves:
                    if reference[1] == valve:
                        if reference[5] == "Solenoid":
                            registers.append("1\nDEV_NAME=Y" + str(reference[6]))
                        else:
                            registers.append("1\nDEV_NAME=M" + str(reference[3]))
        if len(registers) > 0:
            self.dataString += f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID=1
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
{"Positions " + (mode["System"] + " - " if type(mode["System"]) is str else "") + mode["Mode name"]}
*)
<PROPERTIES_END>
<ROOTLINK_START>
[LD_NODE]
TYPE=1
DEV_NAME=M{int(mode["M MODE"])}
[END_LD_NODE]
[LD_NODE]
TYPE=1
DEV_NAME=M{int(mode["M PP"])}
[END_LD_NODE]
[LD_NODE]
TYPE=6
LNK_C=2
LNK_L=1
[END_LD_NODE]
"""
            registers.sort()
            for register in registers:
                self.dataString += f"""[LD_NODE]
TYPE={register}
[END_LD_NODE]
"""
            self.dataString += f"""<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=13
DEV_NAME=M{int(mode["M STATE"])}
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""


class PositionOutput():
    def __init__(self,  valve,  df):
        registers = []
        for index,  row in df.iterrows():
            if type(row["Valves open"]) is str:
                if valve[1] in row["Valves open"]:
                    registers.append(row["M MODE"])
                    registers.append(row["M PP"])
#        if len(valve) > 5:
#            for i, bit in enumerate(valve[5:]):
#                registers.append(bit)
#                self.ports.append(PortDescription('M', bit, valve[1] + " - extra OPEN Bit " + str(i+1) + " valve " + valve[0]))
        if len(registers) > 0:
            self.dataString = f"""<NETWORK_START>
<PROPERTIES_START>
NET_ID=1
NET_LABEL=
NET_ACTIVE=TRUE
NET_BOOKMARK=FALSE
NET_MODIFY=FALSE
NET_FOLD=TRUE
(*
Outputs valve {str(valve[0])} - {str(valve[1])} - {valve[7]}
*)
<PROPERTIES_END>
<ROOTLINK_START>
"""
            for register in registers:
                self.dataString += f"""[LD_NODE]
TYPE=1
DEV_NAME=M{register}
[END_LD_NODE]
"""
            self.dataString += f"""[LD_NODE]
TYPE=6
LNK_C={len(registers)}
LNK_L=1
[END_LD_NODE]
<ROOTLINK_END>
<OUTLINK_START>
[LD_NODE]
TYPE=13
DEV_NAME=M{valve[4]}
[END_LD_NODE]
<OUTLINK_END>
<NETWORK_END>
"""



#TODO: PP Positions active

def makeChecks(fileName):
    df = readModesExcel(fileName)
    modes = []
    for index,  row in df.iterrows():
        if type(row["Mode name"]) is str:
            modes.append(PositionCheck(valveBits(fileName),  row))
    return modes

def makeValvePositions(fileName):
    df = readModesExcel(fileName)
    positions = []
    for valve in valveBits(fileName):
        output = PositionOutput(valve, df)
        if hasattr(output, 'dataString'):
            positions.append(output)
    return positions
    

def valveBits(fileName):
    df = readValvesExcel(fileName)
    bits = []
    indeces = []
    for index,  row in df.iterrows():
        try:
            int(row.Y)
            indeces.append(index)
        except ValueError:
            continue
    noColumns = 0
    for columnName in df.columns:
        if "M extra program" in columnName:
            noColumns += 1
    for index in indeces:
        thisBit = []
        thisBit.append(index)
        thisBit.append(df.at[index, "V"])
        thisBit.append(df.at[index, "M status closed"])
        thisBit.append(df.at[index, "M status open"])
        thisBit.append(df.at[index, "M program set"])
        thisBit.append(df.at[index, "Type"])
        thisBit.append(str(int(df.at[index, "Y"])))
        thisBit.append(df.at[index, "Description"])
#        for i in range(noColumns):
#            if not pd.isnull(df.at[index, "M extra program " + str(i+1)]):
#                thisBit.append(int(df.at[index, "M extra program " + str(i+1)]))
        bits.append(thisBit)
    return bits


#print(valveBits("Devices Plunge pool"))
