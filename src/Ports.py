from src.readISP import PortDescription

def readPorts():
    lines = []
    with open('.\\src\\TemplateComments.rcm') as f:
        lines = f.readlines()
    commentStart = 0
    for i, line in enumerate(lines[:10]):
        if 'C0::' in line:
            commentStart = i
            break
    head = lines[0:commentStart]
    portLines = lines[commentStart:]
    ports = []
    for port in portLines:
        split = port.partition('::')
        group = port[0]
        ports.append(PortDescription(group, int(split[0][1:]), split[2]))
    return head, ports
    
def updatePD(ports, pd):
    for i, port in enumerate(ports):
        if port.letter == pd.letter and port.id == pd.id:
            #print(f'Appending port {pd.letter}{pd.id}')
            ports[i].text = pd.text + (('\n' if pd.text[-1] != '\n' else '') if pd.text != '' else '\n')
    
def writePorts(head, ports):
    with open('.\\tmp\\CommentOutput.rcm', 'w') as f:
        f.writelines(head)
        for port in ports:
            f.writelines([str(port)])
    
