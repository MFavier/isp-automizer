from src.rwExcel import readValvesExcel
from src.readISP import PortDescription
from src.Devices import FunctionBlock

alphabet = ('A',  'B',  'C',  'D',  'E',  'F',  'G',  'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',  'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',  'X',  'Y',  'Z',  'A2',  'B2',  'C2',  'D2',  'E2',  'F2',  'G2',  'H2',  'I2',  'J2',  'K2',  'L2',  'M2',  'N2',  'O2',  'P2',  'Q2',  'R2',  'S2',  'T2',  'U2',  'V2',  'W2',  'X2',  'Y2',  'Z2')
import math

#Todo: solenoid valves, valves with only feedback
class Valve(FunctionBlock):
    def __init__(self,  id,  df):
        FunctionBlock.__init__(self)
        self.ports = []
        self.id = id
        self.type = df.at[self.id,  "Type"]
        self.index = alphabet.index(self.id) + 1
        self.hasOutput = not math.isnan(df.at[self.id,  "Y"])
        self.closedFeedback = not math.isnan(df.at[self.id,  "X closed"])
        self.openFeedback = not math.isnan(df.at[self.id,  "X open"])
        if self.hasOutput:
            self.outputs.append(PortDescription('Y', int(df.at[self.id,  "Y"]), df.at[self.id,  "V"] + " - valve " + self.id))
        self.longName = "Valve " + str(self.id) + " - " + df.at[self.id,  "V"]
        self.inputs.append(PortDescription('M', df.at[self.id,  "M manual closed"], "Valve " + self.id + " CLOSE - " + df.at[self.id,  "V"]))
        self.inputs.append(PortDescription('M', df.at[self.id,  "M manual open"], "Valve " + self.id + " OPEN - " + df.at[self.id,  "V"]))
        self.inputs.append(PortDescription('M', df.at[self.id,  "M auto set"], "Valve " + self.id + " AUTO - " + df.at[self.id,  "V"]))
        self.outputs.append(PortDescription('M', df.at[self.id,  "M manual status"], "Valve " + self.id + " Manual - " + df.at[self.id,  "V"]))
        if self.type == 'Motor_valve':
            self.outputs.append(PortDescription('M', df.at[self.id,  "M close timeout"], "Valve " + self.id + " close timeout - " + df.at[self.id,  "V"]))
            self.outputs.append(PortDescription('M', df.at[self.id,  "M open timeout"], "Valve " + self.id + " open timeout - " + df.at[self.id,  "V"]))
            self.outputs.append(PortDescription('M', df.at[self.id,  "M status closed"], df.at[self.id,  "V"] + " closed - valve " + self.id))
            self.outputs.append(PortDescription('M', df.at[self.id,  "M status open"], df.at[self.id,  "V"] + " open - valve " + self.id))
        self.inputs.append(PortDescription('M', df.at[self.id,  "M program set"], "Valve " + self.id + " Prog - " + df.at[self.id,  "V"]))
        self.name = df.at[self.id,  "V"]
        if self.type == 'Motor_valve' or self.closedFeedback:
            self.inputs.append(PortDescription('X', int(df.at[self.id,  "X closed"]), "Valve " + self.id + " status CLOSED - " + df.at[self.id,  "V"]))
        if self.type == 'Motor_valve' or self.openFeedback:
            self.inputs.append(PortDescription('X', int(df.at[self.id,  "X open"]), "Valve " + self.id + " status OPEN - " + df.at[self.id,  "V"]))
        self.outputs.append(PortDescription('D', df.at[self.id,  "D status"], "Status " + self.longName))
        self.description = self.name +" - Valve " + self.id + "\n" + str(df.at[self.id,  "Description"]) + "\n" + self.type
        self.ports = self.inputs + self.outputs
        self.makeBlock()

def makeValves(fileName):
    df = readValvesExcel(fileName)
    valves = []
    for index,  row in df.iterrows():
        if (not math.isnan(row["Y"])) or (not math.isnan(row["X closed"])) or (not math.isnan(row["X open"])):
            valves.append(Valve(index, df))
    return valves

def valveString(valves):
    data = ""
    for valve in valves:
        data = data + valve.dataString
    return data

def valveVarString(valves):
    data = "<VAR>\n"
    for valve in valves:
        data = data + valve.varString
    data = data + "</VAR>\n"
    return data
