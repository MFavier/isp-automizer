import subprocess

def readMPU(fileName):
    output = []
    with open(".\\data\\" + fileName + '.MPU',  "rb") as input:
        while(byte := input.read(1)):
            output.append(int.from_bytes(byte))
            
    with open('.\\tmp\\' + fileName + '.zip', 'wb') as f: 
        f.write(bytes(output[152:]))

    subprocess.run(['.\\bin\\unzip', '-oq', '-P', '1234', '.\\tmp\\' + fileName + '.zip', '-d', '.\\tmp'])
    with open('.\\tmp\\Unzipped.src') as f:
        lines = f.readlines()
    return lines

#Represents a port description
class PortDescription:
    def __init__(self, letter,  id,  text):
        self.letter = letter
        self.id = id
        self.text = text
    def __repr__(self):
        return self.letter + str(self.id) + "::" + self.text
    def __str__(self):
        return self.letter + str(self.id) + "::" + self.text

def isConsecutive(list):
    for i,  item in enumerate(list):
        if i == 0:
            continue
        else:
            if list[i] != list[i-1] + 1:
                return False
    return True
