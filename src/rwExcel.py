import pandas as pd
 
#Read devices xlsx document and returns Valve table
def readValvesExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx",  sheet_name="Valves", engine='openpyxl', index_col=0,  dtype={'V': str}) 
    #print(df.at["D", "Description"])
    return df

def readDevicesExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx",  sheet_name="Other devices", engine='openpyxl', dtype={'Device ID': str}) 
    #print(df.at["D", "Description"])
    return df

def readModesExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx",  sheet_name="Modes", usecols='A:I', engine='openpyxl',  dtype={'Program':str,  'Mode name':str,  'System':str, 'Valves open': str,  'Valves closed':str, 'M PP':str, 'M PROG':str, 'M MODE':str, 'M STATE':str}) 
    #Dont read comments
    return df

def readAnalogExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx",  sheet_name="Analog inputs", engine='openpyxl', dtype={'ID': str}) 
    #print(df.at["D", "Description"])
    return df

def readAnalogOutputsExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx", sheet_name="Analog outputs", engine='openpyxl', dtype={'ID':str})
    return df
    
def readDigitalInputsExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx", sheet_name="Digital inputs", engine='openpyxl', dtype={'ID':str})
    return df

def readDigitalOutputsExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx", sheet_name="Digital outputs", engine='openpyxl', dtype={'ID':str})
    return df
    
def readLevelsExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx", sheet_name="Levels", engine='openpyxl')
    return df

def readFreqExcel(fileName):
    df = pd.read_excel(fileName + ".xlsx", sheet_name="Freqpumps", engine='openpyxl')
    return df
