import subprocess
import os


def makeZip():
    header = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 252, 3, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, 0, 0]

    fileName = '.\\tmp\\PythonZip' + '.MPU'
    if not os.path.exists('.\\tmp\\Unzipped.src'):
        raise Exception("No source file found")
    if os.path.exists(fileName):
        os.remove(fileName)
    subprocess.run(['.\\bin\\zip', '-qj', '-P', '1234', '.\\' + fileName, '.\\tmp\\Unzipped.src'])
    output = []
    with open(fileName,  "rb") as input:
        while(byte := input.read(1)):
            output.append(int.from_bytes(byte,  'big'))
    localFileHeader = output[0:42] #Always this long because we always zip only Unzipped.src
    localFileHeader[6] = 11 #General purpose bit flag
    if localFileHeader[28] < 256:
        dataOffset = localFileHeader[28]
    else:
        raise ValueError('Header size unexpectedly large')
    localFileHeader[28] = 0

    #Find signatures
    dataDescriptorStart = 0
    centralDirectoryStart = 0
    eocdStart = 0
    for index, byte in enumerate(output): 
        if byte == 80 and output[index+1] == 75:
            if output[index+2] == 7 and output[index+3] == 8:
                dataDescriptorStart = index
            elif output[index+2] == 1 and output[index+3] == 2:
                centralDirectoryStart = index
            elif output[index+2] == 5 and output[index+3] == 6:
                eocdStart = index
    if dataDescriptorStart == 0 or centralDirectoryStart == 0 or eocdStart == 0:
        raise Exception('Cant find valid zip file')

    data = output[42+dataOffset:dataDescriptorStart]
    dataDescriptor = output[dataDescriptorStart:centralDirectoryStart]

    centralDirectory = output[centralDirectoryStart:centralDirectoryStart+58]
    centralDirectory[4] = 20
    centralDirectory[8] = 11 #General purpose bit flag
    centralDirectory[30] = 0
    centralDirectory[38] = 32
    centralDirectory[40] = 0
    centralDirectory[41] = 0
    eocd = output[eocdStart:] 
    eocd[12] = len(centralDirectory)
    newCDStart = int(len(localFileHeader + data + dataDescriptor)).to_bytes(length=2, byteorder='little', signed=True)
    eocd[16] = newCDStart[0]
    eocd[17] = newCDStart[1]

    zipf = localFileHeader + data + dataDescriptor + centralDirectory + eocd
    zipSize = len(zipf)
    zipBits = int(zipSize).to_bytes(length=2, byteorder='little', signed=True)
    header[-4] = zipBits[0]
    header[-3] = zipBits[1]
    newOutput = header + zipf
    with open(fileName, 'wb') as f: 
        f.write(bytes(newOutput))
